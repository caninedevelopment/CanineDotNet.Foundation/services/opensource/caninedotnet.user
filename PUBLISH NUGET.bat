set /p ver="Version: "
dotnet build .\CanineDotNet.User.sln -o Artifacts/%ver% -p:Version=%ver%
dotnet nuget push .\Artifacts\%ver%\*.%ver%.nupkg --source Monosoft