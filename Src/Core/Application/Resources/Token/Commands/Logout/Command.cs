namespace CanineDotNet.User.Application.Resources.Token.Commands.Logout
{
    using CanineDotNet.User.Application.Common.BaseDTO;
    using CanineDotNet.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using CanineDotNet.User.Application.Common.Services;
    using Newtonsoft.Json;

    public class Command : IProcedure<Request>
    {
        private readonly ITokenRepository tokenRepo;
        private readonly IEventService eventService;

        public Command(
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
        }

        public void Execute(Request input)
        {
            var token = tokenRepo.GetById(new Guid(input.Token));

            if (token != null)
            {
                token.ValidUntil = DateTime.Now;
                tokenRepo.Update(token);

                //TODO: Virkelig skidt, dette er rabbitMQ specifikt!                 eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, JsonConvert.SerializeObject(new BaseTokenDTO(token)));
            }
            else
            {
                throw new ElementDoesNotExistException("Uknown token", input.Token);
            }
        }
    }
}