namespace CanineDotNet.User.Application.Resources.Token.Commands.Login
{
    using CanineDotNet.User.Application.Common.BaseDTO;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;

    public class Response : BaseTokenDTO, IDtoResponse
    {
        public Response(Token token)
            :base(token)
        {

        }
    }
}