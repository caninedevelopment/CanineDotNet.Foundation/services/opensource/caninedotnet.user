namespace CanineDotNet.User.Application.Resources.Token.Commands.Verify
{
    using Monosoft.Common.Command.Interfaces;
    using Domain.Entities;
    using CanineDotNet.User.Application.Common.BaseDTO;

    public class Response : BaseTokenDTO, IDtoResponse
    {
        public Response(Token token)
            : base(token)
        {

        }
    }
}