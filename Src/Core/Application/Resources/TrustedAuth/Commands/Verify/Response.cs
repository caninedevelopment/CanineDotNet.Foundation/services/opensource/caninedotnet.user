namespace CanineDotNet.User.Application.Resources.TrustedAuth.Commands.Verify
{
    using CanineDotNet.User.Application.Common.BaseDTO;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;

    public class Response : BaseTokenDTO, IDtoResponse
    {
        public Response(Token token)
            : base(token)
        {
        }
    }
}