﻿namespace CanineDotNet.User.Domain.Interfaces
{
    using CanineDotNet.User.Domain.Entities;

    public interface IUserLoginLogRepository
    {
        void Insert(UserLoginLog log);

    }
}
