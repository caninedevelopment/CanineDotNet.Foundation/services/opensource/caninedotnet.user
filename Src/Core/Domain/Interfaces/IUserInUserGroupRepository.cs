﻿namespace CanineDotNet.User.Domain.Interfaces
{
    using CanineDotNet.User.Domain.Entities;
    using System;
    using System.Collections.Generic;

    public interface IUserInUserGroupRepository
    {
        void Insert(UserInUserGroup uinug);
        List<Guid> GetUserGroupIds(Guid userId);
        UserInUserGroup GetByUserAndUserGroup(Guid userId, Guid usergroupId);
        List<UserInUserGroup> GetByUserGroupId(Guid ugId);
        void DeleteByUserGroup(Guid ugId);
        void Delete(UserInUserGroup uinug);
    }
}
