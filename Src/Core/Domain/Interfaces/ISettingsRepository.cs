﻿namespace CanineDotNet.User.Domain.Interfaces
{
    using CanineDotNet.User.Domain.Entities;

    public interface ISettingsRepository
    {
        Settings GetSettings();
        void SetSettings(Settings set);
    }
}
