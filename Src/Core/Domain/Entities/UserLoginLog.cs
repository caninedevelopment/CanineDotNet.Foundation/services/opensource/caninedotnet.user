﻿namespace CanineDotNet.User.Domain.Entities
{
    using System;

    public class UserLoginLog
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Ip { get; set; }
        public bool Success { get; set; }
        public Guid UserId { get; set; }

        public UserLoginLog() { }

        public UserLoginLog(DateTime datetime, string ip, bool success, Guid userId)
        {
            this.Id = Guid.NewGuid();
            this.Date = datetime;
            this.Ip = ip;
            this.Success = success;
            this.UserId = userId;
        }
    }
}
