﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;

    public class User
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string PWDHash { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string ResetToken { get; set; }

        public string MetadataString { get; set; }

        public Domain.Entities.User AsDomain()
        {
            return new Domain.Entities.User() 
            { 
                Id = this.Id, 
                Username = this.Username, 
                PWDHash = this.PWDHash, 
                Email = this.Email, 
                Mobile = this.Mobile, 
                ResetToken = this.ResetToken, 
                MetadataString = this.MetadataString 
            };

        }

        public User() { }
        public User(Domain.Entities.User user)
        {
            this.Id = user.Id;
            this.Username = user.Username;
            this.PWDHash = user.PWDHash;
            this.Email = user.Email;
            this.Mobile = user.Mobile;
            this.ResetToken = user.ResetToken;
            this.MetadataString = user.MetadataString;
        }

    }
}
