﻿using System;

namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    public class Settings
    {
        public Guid Id { get; set; }
        public int SMTPPort { get; set; }
        public string SMTPServerName { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }

        public string ForgotPasswordEmailSubject { get; set; }
        public string ForgotPasswordEmailBody { get; set; }
        public string ForgotPasswordSenderEmail { get; set; }
        public string ForgotPasswordNameOfSender { get; set; }

        public string AnonymUserGroup { get; set; }

        public Domain.Entities.Settings AsDomain()
        {
            return new Domain.Entities.Settings() { 
             Id = this.Id,
             SMTPPort = this.SMTPPort,
             SMTPServerName = this.SMTPServerName,
             SMTPUserName = this.SMTPUserName,
             SMTPPassword = this.SMTPPassword,
             ForgotPasswordEmailSubject = this.ForgotPasswordEmailSubject,
             ForgotPasswordEmailBody = this.ForgotPasswordEmailBody,
             ForgotPasswordNameOfSender = this.ForgotPasswordNameOfSender,
             ForgotPasswordSenderEmail = this.ForgotPasswordSenderEmail,
             AnonymUserGroup = this.AnonymUserGroup
            };
        }

    }
}
