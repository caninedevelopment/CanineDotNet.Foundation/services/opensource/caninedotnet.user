﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;

    public class UserLoginLog
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Ip { get; set; }
        public bool Success { get; set; }
        public Guid FK_User { get; set; }

        public Domain.Entities.UserLoginLog AsDomain()
        {
            return new Domain.Entities.UserLoginLog() { Id = this.Id, Date = this.Date, Ip = this.Ip, Success = this.Success, UserId = this.FK_User };
        }


        public UserLoginLog() { }

        public UserLoginLog(Domain.Entities.UserLoginLog userIngroup)
        {
            this.Id = userIngroup.Id;
            this.Date = userIngroup.Date;
            this.Ip = userIngroup.Ip;
            this.Success = userIngroup.Success;
            this.FK_User = userIngroup.UserId;
        }
    }
}
