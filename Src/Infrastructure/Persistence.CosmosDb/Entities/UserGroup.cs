﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserGroup
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Claims { get; set; }

        public Domain.Entities.UserGroup AsDomain()
        {
            return new Domain.Entities.UserGroup() { Id = this.Id, Name = this.Name, Claims = this.GetClaimList().ToList()};
        }

        public string[] GetClaimList()
        {
            return this.Claims.Split(',');
        }

        public UserGroup()
        { }

        public UserGroup(Domain.Entities.UserGroup usergroup)
        {
            this.Id = usergroup.Id;
            this.Name = usergroup.Name;
            this.Claims = string.Join(",", usergroup.Claims);
        }

    }
}
