﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Token
    {
        public Guid Id { get; set; }
        public Guid FK_User { get; set; }
        public DateTime ValidUntil { get; set; }
        public string Claims { get; set; }

        public Domain.Entities.Token AsDomain ()
        {
            return new Domain.Entities.Token(this.Id, this.FK_User, this.ValidUntil, this.GetClaimList().ToList());
        }
        public string[] GetClaimList()
        {
            return this.Claims.Split(',');
        }

        public Token() { }
        public Token(Domain.Entities.Token token) 
        {
            this.Id = token.Id;
            this.FK_User = token.FK_User;
            this.ValidUntil = token.ValidUntil;
            this.Claims = string.Join(",", token.Claims);
        }
    }
}
