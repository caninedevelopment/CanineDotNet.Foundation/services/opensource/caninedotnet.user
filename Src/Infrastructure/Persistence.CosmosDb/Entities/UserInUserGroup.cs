﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;
    
    public class UserInUserGroup
    {
        public Guid Id { get; set; }

        public Guid FK_User { get; set; }

        public Guid FK_UserGroup { get; set; }

        public Domain.Entities.UserInUserGroup AsDomain()
        {
            return new Domain.Entities.UserInUserGroup() { Id = this.Id, FK_User = this.FK_User, FK_UserGroup = this.FK_UserGroup};
        }

        public UserInUserGroup() { }

        public UserInUserGroup(Domain.Entities.UserInUserGroup userIngroup)
        {
            this.Id = userIngroup.Id;
            this.FK_User = userIngroup.FK_User;
            this.FK_UserGroup = userIngroup.FK_UserGroup;
        }
    }
}
