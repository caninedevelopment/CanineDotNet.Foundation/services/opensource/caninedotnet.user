﻿namespace CanineDotNet.User.Persistence.CosmosDb.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrustedAuth
    {
        public Guid Id { get; set; }
        public string ServerUrl { get; set; }
        public string Trust { get; set; }

        public Domain.Entities.TrustedAuth AsDomain()
        {
            return new Domain.Entities.TrustedAuth() { Id = this.Id, ServerUrl = this.ServerUrl, Trust = this.GetTrustList().ToList() };
        }
        public string[] GetTrustList()
        {
            return this.Trust.Split(',');
        }

        public TrustedAuth() { }

        public TrustedAuth(Domain.Entities.TrustedAuth auth)
        {
            this.Id = auth.Id;
            this.ServerUrl = auth.ServerUrl;
            this.Trust = string.Join(",", auth.Trust);
        }
    }
}
