﻿namespace CanineDotNet.User.Persistence.CosmosDb.Repositories
{
    using CanineDotNet.User.Domain.Interfaces;
    using CanineDotNet.User.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserRepository : IUserRepository
    {
        private readonly DbContext db;

        public UserRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(User user)
        {
            db.Users.Remove(new Entities.User( user));
            db.SaveChanges();
        }

        public List<User> GetAll()
        {
            return db.Users.Select(p => p.AsDomain()).ToList();
        }

        public User GetByEmail(string email)
        {
            return db.Users.Where(x => x.Email == email).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public User GetById(Guid id)
        {
            return db.Users.Where(x => x.Id == id).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public User GetByResetToken(string token)
        {
            return db.Users.Where(x => x.ResetToken == token).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public User GetByUsername(string username)
        {
            return db.Users.Where(x => x.Username == username).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public void Insert(User user)
        {
            db.Users.Add(new Entities.User( user));
            db.SaveChanges();
        }

        public void Update(User user)
        {
            db.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }
    }
}