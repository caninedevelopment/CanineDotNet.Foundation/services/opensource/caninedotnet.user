﻿namespace CanineDotNet.User.Persistence.CosmosDb.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;

    public class UserLoginLogRepository : IUserLoginLogRepository
    {
        private readonly DbContext db;

        public UserLoginLogRepository(DbContext db)
        {
            this.db = db;
        }

        public void Insert(UserLoginLog log)
        {
            db.UserLoginLogs.Add(new Entities.UserLoginLog( log));
            db.SaveChanges();
        }
    }
}
