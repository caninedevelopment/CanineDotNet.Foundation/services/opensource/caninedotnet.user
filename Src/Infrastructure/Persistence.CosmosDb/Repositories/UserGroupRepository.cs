﻿namespace CanineDotNet.User.Persistence.CosmosDb.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserGroupRepository : IUserGroupRepository
    {
        private readonly DbContext db;

        public UserGroupRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(UserGroup usergroup)
        {
            db.Remove(usergroup);
            db.SaveChanges();
        }

        public List<UserGroup> GetAll()
        {
            return db.Usergroups.Select(p=>p.AsDomain()).ToList();
        }

        public UserGroup GetById(Guid id)
        {
            return db.Usergroups.Where(x => x.Id == id).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public List<UserGroup> GetByIds(List<Guid> ugIds)
        {
            var ugs = db.Usergroups.Where(x => ugIds.Contains(x.Id)).Select(p => p.AsDomain()).ToList();
            return ugs;
        }

        public void Insert(UserGroup usergroup)
        {
            db.Usergroups.Add(new Entities.UserGroup( usergroup));
            db.SaveChanges();
        }

        public void Update(UserGroup usergroup)
        {
            db.Entry(usergroup).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }
    }
}
