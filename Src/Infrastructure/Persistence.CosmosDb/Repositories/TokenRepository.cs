﻿namespace CanineDotNet.User.Persistence.CosmosDb.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TokenRepository : ITokenRepository
    {
        private readonly DbContext db;

        public TokenRepository(DbContext db)
        {
            this.db = db;
        }

        public Token GetById(Guid id)
        {
            return db.Tokens.Where(x => x.Id == id).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public List<Token> GetByUserIdFromDateTime(Guid userId, DateTime fromDateTime)
        {
            return db.Tokens.Where(x => x.FK_User == userId && x.ValidUntil > fromDateTime).Select(p=> p.AsDomain()).ToList();
        }

        public void Insert(Token token)
        {
            db.Tokens.Add(new Entities.Token(token));
            db.SaveChanges();
        }

        public void Update(Token token)
        {
            //TODO
//            db.Entry(new Entities.Token(token)).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
//            db.SaveChanges();
        }
    }
}
