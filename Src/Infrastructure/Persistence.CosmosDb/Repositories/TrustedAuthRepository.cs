﻿namespace CanineDotNet.User.Persistence.CosmosDb.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;
    using System.Linq;

    public class TrustedAuthRepository : ITrustedAuthRepository
    {
        private readonly DbContext db;

        public TrustedAuthRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(string url)
        {
            var entity = db.TrustedAuths.Where(x => x.ServerUrl == url).FirstOrDefault();
            if (entity != null)
            {
                db.TrustedAuths.Remove(entity);
                db.SaveChanges();
            }
        }

        public TrustedAuth GetByUrl(string url)
        {
            return db.TrustedAuths.Where(x => x.ServerUrl == url).Select(p => p.AsDomain()).FirstOrDefault();
        }

        public void Insert(TrustedAuth trustedauth)
        {
            db.TrustedAuths.Add(new Entities.TrustedAuth(trustedauth));
            db.SaveChanges();
        }
    }
}
