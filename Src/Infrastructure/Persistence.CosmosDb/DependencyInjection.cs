namespace CanineDotNet.User.Persistence.CosmosDb
{
    using CanineDotNet.User.Domain.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using CanineDotNet.User.Persistence.CosmosDb.Repositories;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;

    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistenceProvider(this IServiceCollection services, CosmosDbSettings cosmosSettings)
        {
            services.AddDbContext<DbContext>(options => options.UseCosmos(cosmosSettings.accountendpoint, cosmosSettings.accountkey, cosmosSettings.databasename), 
                ServiceLifetime.Scoped);
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<IUserInUserGroupRepository, UserInUserGroupRepository>();
            services.AddScoped<ITokenRepository, TokenRepository>();
            services.AddScoped<ISettingsRepository, SettingsRepository>();
            services.AddScoped<IUserLoginLogRepository, UserLoginLogRepository>();
            services.AddScoped<ITrustedAuthRepository, TrustedAuthRepository>();

            return services;
        }
    }
}