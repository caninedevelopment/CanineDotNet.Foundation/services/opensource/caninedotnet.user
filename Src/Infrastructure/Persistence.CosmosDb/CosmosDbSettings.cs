﻿namespace CanineDotNet.User.Persistence.CosmosDb
{
    public class CosmosDbSettings
    {
        public string accountendpoint { get; set; }
        public string accountkey { get; set; }
        public string databasename { get; set; }
    }
}