﻿namespace CanineDotNet.User.Persistence.PostgreSQL.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;
    using System.Linq;

    public class SettingsRepository : ISettingsRepository
    {
        private readonly DbContext db;

        public SettingsRepository(DbContext db)
        {
            this.db = db;
        }

        public Settings GetSettings()
        {
            return db.Settings.FirstOrDefault();
        }

        public void SetSettings(Settings set)
        {
            var oldSet = db.Settings.FirstOrDefault();
            if (oldSet != null)
                db.Entry(set).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            else
                db.Entry(set).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            db.SaveChanges();
        }
    }
}
