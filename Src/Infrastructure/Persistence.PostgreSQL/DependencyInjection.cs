namespace CanineDotNet.User.Persistence.PostgreSQL
{
    using CanineDotNet.User.Domain.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using CanineDotNet.User.Persistence.PostgreSQL.Repositories;
    using Microsoft.EntityFrameworkCore;

    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            services.AddDbContext<DbContext>(options => options.UseNpgsql($"User ID={dbUser};Password={dbPassword};Host={dbServerHost};Port={dbServerPort};Database={dbName};"));

            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IUserGroupRepository, UserGroupRepository>();
            services.AddSingleton<IUserInUserGroupRepository, UserInUserGroupRepository>();
            services.AddSingleton<ITokenRepository, TokenRepository>();
            services.AddSingleton<ISettingsRepository, SettingsRepository>();
            services.AddSingleton<IUserLoginLogRepository, UserLoginLogRepository>();
            services.AddSingleton<ITrustedAuthRepository, TrustedAuthRepository>();

            return services;
        }
    }
}