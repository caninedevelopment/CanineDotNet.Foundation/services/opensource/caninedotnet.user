namespace CanineDotNet.User.Implementation
{
    using Microsoft.Extensions.DependencyInjection;
    using CanineDotNet.User.Application.Common.Services;
    using CanineDotNet.User.Implementation.Services;

    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services)
        {
            services.AddSingleton<IEventService, EventService>();

            return services;
        }
    }
}