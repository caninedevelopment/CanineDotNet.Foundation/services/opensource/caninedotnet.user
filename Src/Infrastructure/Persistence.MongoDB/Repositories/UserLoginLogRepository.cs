﻿namespace CanineDotNet.User.Persistence.MongoDB.Repositories
{
    using CanineDotNet.User.Domain.Entities;
    using CanineDotNet.User.Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UserLoginLogRepository : IUserLoginLogRepository
    {
        private readonly DbContext context;

        public UserLoginLogRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "userloginlog";

        private IMongoCollection<UserLoginLog> collection
        {
            get { return this.context.db.GetCollection<UserLoginLog>(collectionName); }
        }

        public void Insert(UserLoginLog log)
        {
            collection.InsertOne(log);
        }
    }
}
