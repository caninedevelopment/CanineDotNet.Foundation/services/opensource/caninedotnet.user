namespace CanineDotNet.User.AzureFunction.CosmosDb
{
    using CanineDotNet.User.Application.Common.Services;
    using CanineDotNet.User.Domain.Interfaces;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System;
    using System.Threading.Tasks;

    public class User
    {
        private IUserRepository userRepo { get; set; }
        private IUserLoginLogRepository logRepo { get; set; }
        private IUserInUserGroupRepository uinugRepo { get; set; }
        private IUserGroupRepository ugRepo { get; set; }
        private ITokenRepository tokenRepo { get; set; }
        private IEventService eventService { get; set; }

        public User(
            IUserRepository userRepo,
            IUserLoginLogRepository logRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.userRepo = userRepo;
            this.logRepo = logRepo;
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
        }

        [FunctionName("user-GetAll")]
        public async Task<IActionResult> GetAll(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "user/v1/getall")] HttpRequest req,
            ILogger log
            )
        {
            var result = new CanineDotNet.User.Application.Resources.User.Commands.GetAll.Command(userRepo, uinugRepo, ugRepo).Execute();
            return new OkObjectResult(result);
        }

        [FunctionName("user-Get")]
        public async Task<IActionResult> Get(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "user/v1/get/{id}")] HttpRequest req,
            ILogger log,
            Guid id
            )
        {
            var result = new CanineDotNet.User.Application.Resources.User.Commands.Get.Command(userRepo, uinugRepo, ugRepo).Execute(new CanineDotNet.User.Application.Resources.User.Commands.Get.Request() {  UserId = id});
            return new OkObjectResult(result);
        }

        //TODO: HANDLE TOKEN/SECURITY
        //[FunctionName("GetMyData")]
        //public async Task<IActionResult> GetMyData(
        //    [HttpTrigger(AuthorizationLevel.Function, "get", Route = "user/v1/getMyData")] HttpRequest req,
        //    ILogger log,
        //    Guid id
        //    )
        //{
        //    var result = new CanineDotNet.User.Application.Resources.User.Commands.GetMyData.Command(userRepo, uinugRepo, ugRepo).Execute(new CanineDotNet.User.Application.Resources.User.Commands.GetMyData.Request() { User, Token });
        //    return new OkObjectResult(result);
        //}



        [FunctionName("user-Update")]
        public async Task<IActionResult> Update(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "user/v1/update")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.User.Commands.Update.Request>(body);
            new CanineDotNet.User.Application.Resources.User.Commands.Update.Command(userRepo).Execute(input);
            return new OkObjectResult(null);
        }

        [FunctionName("user-Insert")]
        public async Task<IActionResult> Insert(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "user/v1/insert")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.User.Commands.Insert.Request>(body);
            new CanineDotNet.User.Application.Resources.User.Commands.Insert.Command(userRepo).Execute(input);
            return new OkObjectResult(null);
        }

        [FunctionName("user-Delete")]
        public async Task<IActionResult> Delete(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "user/v1/delete")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.User.Commands.Delete.Request>(body);
            new CanineDotNet.User.Application.Resources.User.Commands.Delete.Command(userRepo).Execute(input);
            return new OkObjectResult(null);
        }

        //            this.AddCommand(new Command.AnonymCreate.Command(setRepo, uRepo, uinugRepo), OperationType.Execute, "AnonymCreate", "Create new user and send mail to email", Claim.IsOperator);
        //            this.AddCommand(new Command.ChangePassword.Command(uRepo), OperationType.Execute, "ChangePasword", "Change your own password", Claim.IsConsumer);
        //            this.AddCommand(new Command.ForgotPassword.Command(uRepo, setRepo), OperationType.Execute, "ForgotPassword", "Reset password, send mail to user with reset token");
        //            this.AddCommand(new Command.ResetPassword.Command(uRepo), OperationType.Execute, "ResetPassword", "Reset password, and return reset token");
        //            this.AddCommand(new Command.ResetPasswordByToken.Command(uRepo), OperationType.Execute, "ResetPasswordByToken", "Use reset token to create new password");
        //            this.AddCommand(new Command.Setup.Command(setRepo), OperationType.Execute, "Setup", "Setup user service, for SMTP, what group annonyme user should have..", Claim.IsAdmininistrator);
        //            this.AddCommand(new Command.UpdateMyData.Command(uRepo), OperationType.Execute, "UpdateMyData", "Update own user data", Claim.IsConsumer);

    }
}


