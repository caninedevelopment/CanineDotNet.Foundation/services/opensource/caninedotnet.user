namespace CanineDotNet.User.AzureFunction.CosmosDb
{
    using CanineDotNet.User.Application.Common.Services;
    using CanineDotNet.User.Domain.Interfaces;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System.Threading.Tasks;

    public class UserGroup
    {
        private IUserInUserGroupRepository uinugRepo { get; set; }
        private IUserGroupRepository ugRepo { get; set; }
        private IEventService eventService { get; set; }

        public UserGroup(
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            IEventService eventService)
        {
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
            this.eventService = eventService;
        }

        [FunctionName("usergroup-GetAll")]
        public async Task<IActionResult> GetAll(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "userGroup/v1/getall")] HttpRequest req,
            ILogger log
            )
        {
            var result = new CanineDotNet.User.Application.Resources.UserGroup.Commands.GetAll.Command(ugRepo).Execute();
            return new OkObjectResult(result);
        }

        [FunctionName("usergroup-Update")]
        public async Task<IActionResult> Update(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "userGroup/v1/update")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.UserGroup.Commands.Update.Request>(body);
            new CanineDotNet.User.Application.Resources.UserGroup.Commands.Update.Command(ugRepo, uinugRepo, eventService).Execute(input);
            return new OkObjectResult(null);
        }

        [FunctionName("usergroup-Insert")]
        public async Task<IActionResult> Insert(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "userGroup/v1/insert")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.UserGroup.Commands.Insert.Request>(body);
            new CanineDotNet.User.Application.Resources.UserGroup.Commands.Insert.Command(ugRepo).Execute(input);
            return new OkObjectResult(null);
        }

        [FunctionName("usergroup-Delete")]
        public async Task<IActionResult> Delete(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "userGroup/v1/delete")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.UserGroup.Commands.Delete.Request>(body);
            new CanineDotNet.User.Application.Resources.UserGroup.Commands.Delete.Command(ugRepo, uinugRepo, eventService).Execute(input);
            return new OkObjectResult(null);
        }


        [FunctionName("usergroup-AddUserToUsergroup")]
        public async Task<IActionResult> AddUserToUsergroup(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "userGroup/v1/addUserToUsergroup")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.UserGroup.Commands.AddUserToUserGroup.Request>(body);
            new CanineDotNet.User.Application.Resources.UserGroup.Commands.AddUserToUserGroup.Command(uinugRepo, eventService).Execute(input);
            return new OkObjectResult(null);
        }

        [FunctionName("usergroup-RemoveUserFromUsergroup")]
        public async Task<IActionResult> RemoveUserFromUsergroup(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "userGroup/v1/removeUserFromUsergroup")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.UserGroup.Commands.RemoveUserFromUserGroup.Request>(body);
            new CanineDotNet.User.Application.Resources.UserGroup.Commands.RemoveUserFromUserGroup.Command(uinugRepo, eventService).Execute(input);
            return new OkObjectResult(null);
        }
    }
}

