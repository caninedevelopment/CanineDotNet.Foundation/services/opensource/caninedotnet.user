namespace CanineDotNet.User.AzureFunction.CosmosDb
{
    using CanineDotNet.User.Application.Common.Services;
    using CanineDotNet.User.Domain.Interfaces;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System.Threading.Tasks;

    public class Token
    {

        private IUserRepository userRepo { get; set; }
        private IUserLoginLogRepository logRepo { get; set; }
        private IUserInUserGroupRepository uinugRepo { get; set; }
        private IUserGroupRepository ugRepo { get; set; }
        private ITokenRepository tokenRepo { get; set; }
        private IEventService eventService { get; set; }

        public Token(
            IUserRepository userRepo,
            IUserLoginLogRepository logRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.userRepo = userRepo;
            this.logRepo = logRepo;
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
        }

        [FunctionName("token-Login")]
        public async Task<IActionResult> Login(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "token/v1/login")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.Token.Commands.Login.Request>(body);
            var result = new CanineDotNet.User.Application.Resources.Token.Commands.Login.Command(userRepo, logRepo, uinugRepo, ugRepo, tokenRepo, eventService).Execute(input);

            return new OkObjectResult(result);
        }


        [FunctionName("token-Logout")]
        public async Task<IActionResult> Logout(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "token/v1/logout")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject< CanineDotNet.User.Application.Resources.Token.Commands.Logout.Request> (body);
            new CanineDotNet.User.Application.Resources.Token.Commands.Logout.Command(tokenRepo, eventService).Execute(input);

            return new OkObjectResult(null);
        }


        [FunctionName("token-Refresh")]
        public async Task<IActionResult> Refresh(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "token/v1/refresh")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.Token.Commands.Refresh.Request>(body);
            var result = new CanineDotNet.User.Application.Resources.Token.Commands.Refresh.Command(tokenRepo, eventService).Execute(input);

            return new OkObjectResult(result);
        }

        [FunctionName("token-Verify")]
        public async Task<IActionResult> Verify(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "token/v1/verify")] HttpRequest req,
            ILogger log
            )
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(true);
            var input = JsonConvert.DeserializeObject<CanineDotNet.User.Application.Resources.Token.Commands.Verify.Request>(body);
            var result = new CanineDotNet.User.Application.Resources.Token.Commands.Verify.Command(tokenRepo).Execute(input);

            return new OkObjectResult(result);
        }
    }

}
