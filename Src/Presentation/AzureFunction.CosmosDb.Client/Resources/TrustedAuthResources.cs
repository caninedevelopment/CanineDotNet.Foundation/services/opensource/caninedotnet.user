namespace CanineDotNet.User.AzureFunction.CosmosDb
{
    using CanineDotNet.User.Application.Common.Services;
    using CanineDotNet.User.Domain.Interfaces;
    using CanineDotNet.User.Persistence.CosmosDb;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System.Threading.Tasks;

    public class TrustedAuth
    {

        private IUserRepository userRepo { get; set; }
        private IUserLoginLogRepository logRepo { get; set; }
        private IUserInUserGroupRepository uinugRepo { get; set; }
        private IUserGroupRepository ugRepo { get; set; }
        private ITokenRepository tokenRepo { get; set; }
        private IEventService eventService { get; set; }

        public TrustedAuth(
            IUserRepository userRepo,
            IUserLoginLogRepository logRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.userRepo = userRepo;
            this.logRepo = logRepo;
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
        }

        //TODO
        //        public TrustedAuthResource(
        //            ITrustedAuthRepository taRepo,
        //            ITokenRepository tokenRepo) : base("TrustedAuth")
        //        {
        //            this.AddCommand(new Command.Login.Command(taRepo, tokenRepo), OperationType.Execute, "Login", "Login to external canine");
        //            this.AddCommand(new Command.Register.Command(taRepo), OperationType.Execute, "Register", "Register external canine server", Claim.IsOperator);
        //            this.AddCommand(new Command.Unregister.Command(taRepo), OperationType.Execute, "Unregister", "Remove external canine server", Claim.IsOperator);
        //            this.AddCommand(new Command.Verify.Command(taRepo, tokenRepo), OperationType.Execute, "Verify", "Verify token on external canine", Claim.IsConsumer);
        //        }
    }

}