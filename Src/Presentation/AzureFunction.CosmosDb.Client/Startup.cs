﻿using AzureFunctions.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using CanineDotNet.User.Implementation;
using CanineDotNet.User.Persistence.CosmosDb;

[assembly: FunctionsStartup(typeof(Startup))]
namespace AzureFunctions.Client
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            //BEMÆRK ::: >> HENT CONFIG MED:  >> (kræver at Azure CLI er hentet/konfigureret (se evt. https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli)
            var config = new ConfigurationBuilder()
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            var cosmosSettings = new CosmosDbSettings();
            config.GetSection("cosmosdb").Bind(cosmosSettings);

            builder.Services
                .AddImplementation()
                .AddPersistenceProvider(cosmosSettings)
                .BuildServiceProvider();
        }
    }


}
